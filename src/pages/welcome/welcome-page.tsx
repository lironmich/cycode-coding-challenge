import React, { useContext } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import {
  Button,
  Paper,
  Typography,
  List,
  ListItem,
  ListItemText,
} from "@material-ui/core";
import { ROUTES } from "../../consts/routes";
import { SubscriptionsContext } from "../subscription/subscription-context";

const Page = styled(Paper)`
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const TypographyMargin = styled.div`
  flex: 0 0 20px;
`;

const Subscriptions = styled(Paper)`
  padding: 30px;
`;

function WelcomePage() {
  const { subscriptions } = useContext(SubscriptionsContext);

  const renderSubscriptionLink = () => (
    <>
      <Typography variant="subtitle1">Welcome to the site message</Typography>
      <TypographyMargin />

      <Button
        variant="outlined"
        color="primary"
        component={Link}
        to={ROUTES.Subscribe}
      >
        Subscribe
      </Button>
    </>
  );

  const renderSubscriptions = () => (
    <Subscriptions>
      <Typography variant="subtitle1">You are subscribed to:</Typography>
      <TypographyMargin />

      <List component="nav">
        {subscriptions.map((subscription, index) => (
          <ListItem key={index}>
            <ListItemText primary={subscription} />
          </ListItem>
        ))}
      </List>
    </Subscriptions>
  );

  return (
    <Page>
      {subscriptions.length === 0
        ? renderSubscriptionLink()
        : renderSubscriptions()}
    </Page>
  );
}

export { WelcomePage };
