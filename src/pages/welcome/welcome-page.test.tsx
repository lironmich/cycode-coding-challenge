import React from "react";
import { render, RenderResult } from "@testing-library/react";
import { WelcomePage } from "./welcome-page";
import { BrowserRouter } from "react-router-dom";
import { SubscriptionsContext } from "../subscription/subscription-context";

describe("welcome-page", () => {
  describe("without subscriptions", () => {
    let renderResult: RenderResult;

    beforeEach(() => {
      renderResult = render(
        <BrowserRouter>
          <WelcomePage />
        </BrowserRouter>
      );
    });

    test("should have welcome message", async () => {
      const welcomeMessage = await renderResult.findByText(
        "Welcome to the site message"
      );

      expect(welcomeMessage).toBeTruthy();
    });

    test("should have link to subscription page", async () => {
      const subscriptionText = await renderResult.findByText("Subscribe");
      const subscriptionLink = subscriptionText.closest(
        "a"
      ) as HTMLAnchorElement;

      const href = subscriptionLink.href;

      expect(href).toContain("/subscribe");
    });
  });

  describe("with subscriptions", () => {
    const subscriptions = ["mail", "sms"];

    let renderResult: RenderResult;

    beforeEach(() => {
      const setSubscriptions = () => {};

      renderResult = render(
        <BrowserRouter>
          <SubscriptionsContext.Provider
            value={{
              setSubscriptions,
              subscriptions,
            }}
          >
            <WelcomePage />
          </SubscriptionsContext.Provider>
        </BrowserRouter>
      );
    });

    test("should display subscription title", async () => {
      const subscriptionsTitle = await renderResult.findByText(
        "You are subscribed to:"
      );

      expect(subscriptionsTitle).toBeTruthy();
    });

    test("should display all subscriptions", async () => {
      const firstSubscription = await renderResult.findByText(subscriptions[0]);
      const secondSubscription = await renderResult.findByText(
        subscriptions[1]
      );

      expect(subscriptions.length).toBe(2);
      expect(firstSubscription).toBeTruthy();
      expect(secondSubscription).toBeTruthy();
    });
  });
});
