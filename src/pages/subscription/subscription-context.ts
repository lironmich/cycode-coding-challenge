import React from "react";

interface ISubscriptionsContext {
  subscriptions: string[];
  setSubscriptions: (subscriptions: string[]) => void;
}

export const SubscriptionsContext = React.createContext<ISubscriptionsContext>({
  subscriptions: [],
  setSubscriptions: () => {},
});
