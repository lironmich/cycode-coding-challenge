import React from "react";
import { render, RenderResult } from "@testing-library/react";
import { SubscriptionPage } from "./subscription-page";
import { BrowserRouter } from "react-router-dom";

describe("subscription-page", () => {
  let renderResult: RenderResult;

  const clickSubscribeButton = async () => {
    const subscriptionText = await renderResult.findByText(
      "Subscribe to our channel"
    );

    const subscriptionButton = subscriptionText.closest(
      "button"
    ) as HTMLButtonElement;

    subscriptionButton.click();
  };

  beforeEach(() => {
    renderResult = render(
      <BrowserRouter>
        <SubscriptionPage />
      </BrowserRouter>
    );
  });

  test("clicking on subscription-button should open modal", async () => {
    await clickSubscribeButton();

    const modal = await renderResult.findByTestId("modal-frame");

    expect(modal).toBeTruthy();
  });
});
