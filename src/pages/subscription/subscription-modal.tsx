import React, { useContext, useState } from "react";
import { Modal, Fade, Backdrop, Paper, Button } from "@material-ui/core";
import styled from "styled-components";
import { AutocompleteComponent } from "../../components/autocomplete/autocomplete";
import { SUBSCRIPTIONS } from "../../consts/subscriptions";
import { ROUTES } from "../../consts/routes";
import { Link } from "react-router-dom";
import { SubscriptionsContext } from "./subscription-context";
import { ModalFrameComponent } from "../../components/frame/modal-frame";

interface ISubscriptionModalProps {
  isOpen: boolean;
  onClose: () => void;
}

const Popup = styled(Paper)`
  position: fixed;
  top: 25%;
  right: 25%;
  bottom: 25%;
  left: 25%;
  padding: 20px;
`;

const SubmitButton = styled.div`
  position: absolute;
  bottom: 20px;
  left: 50%;
  transform: translateX(-50%);
`;

function SubscriptionModal(props: ISubscriptionModalProps) {
  const [unsubmittedSubscriptions, setUnsubmittedSubscriptions] = useState<
    string[]
  >([]);
  const { setSubscriptions } = useContext(SubscriptionsContext);

  const onSubscriptionChange = (values: string[]) => {
    setUnsubmittedSubscriptions(values);
  };

  const onSubmit = () => {
    setSubscriptions(unsubmittedSubscriptions);
  };

  return (
    <ModalFrameComponent {...props}>
      <Modal
        open={props.isOpen}
        onClose={props.onClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
        disablePortal={true}
      >
        <Fade in={props.isOpen}>
          <Popup onClick={(e) => e.stopPropagation()}>
            <AutocompleteComponent
              values={unsubmittedSubscriptions}
              maxOptions={3}
              options={SUBSCRIPTIONS}
              onChange={onSubscriptionChange}
            />

            <SubmitButton>
              <Button
                onClick={onSubmit}
                variant="outlined"
                color="primary"
                component={Link}
                to={ROUTES.Welcome}
              >
                Submit
              </Button>
            </SubmitButton>
          </Popup>
        </Fade>
      </Modal>
    </ModalFrameComponent>
  );
}

export { SubscriptionModal };
