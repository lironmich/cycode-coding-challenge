import React, { ReactNode } from "react";
import { render, RenderResult } from "@testing-library/react";
import { SubscriptionModal } from "./subscription-modal";
import { BrowserRouter } from "react-router-dom";

jest.mock("../../components/frame/modal-frame", () => ({
  ModalFrameComponent: (props: { children: ReactNode }) => (
    <div>{props.children}</div>
  ),
}));

describe("subscription-modal", () => {
  let renderResult: RenderResult;

  const clickSubmitButton = async () => {
    const submitText = await renderResult.findByText("Submit");

    const submitButton = submitText.closest("a") as HTMLAnchorElement;

    submitButton.click();
  };

  beforeEach(() => {
    renderResult = render(
      <BrowserRouter>
        <SubscriptionModal isOpen={true} onClose={() => {}} />
      </BrowserRouter>
    );
  });

  test("clicking on submit should navigate to welcome page", async () => {
    await clickSubmitButton();

    expect(window.location.pathname).toBe("/welcome");
  });
});
