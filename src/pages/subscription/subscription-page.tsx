import React, { useState } from "react";
import styled from "styled-components";
import { SubscriptionModal } from "./subscription-modal";
import { Button, Paper } from "@material-ui/core";

const Page = styled(Paper)`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

function SubscriptionPage() {
  const [isSubscriptionOpen, setIsSubscriptionOpen] = useState(false);

  const subscribe = (e: any) => {
    setIsSubscriptionOpen(true);
    e.stopPropagation();
  };

  const unsubscribe = () => {
    setIsSubscriptionOpen(false);
  };

  return (
    <Page onClick={unsubscribe}>
      <Button onClick={subscribe} variant="outlined" color="primary">
        Subscribe to our channel
      </Button>

      <SubscriptionModal isOpen={isSubscriptionOpen} onClose={unsubscribe} />
    </Page>
  );
}

export { SubscriptionPage };
