import { Theme } from "@material-ui/core";

export interface ITheme {
  theme: Theme;
}
