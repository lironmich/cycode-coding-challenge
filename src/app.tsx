import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import styled from "styled-components";
import { WelcomePage } from "./pages/welcome/welcome-page";
import { SubscriptionPage } from "./pages/subscription/subscription-page";
import { SubscriptionsContext } from "./pages/subscription/subscription-context";
import { NavigationBarComponent } from "./components/navigation-bar/navigation-bar";
import { PageTitleComponent } from "./components/page-title/page-title";
import { ThemeProvider, CssBaseline } from "@material-ui/core";
import { defaultTheme } from "./theme/defaule-theme";
import { ROUTES } from "./consts/routes";
import { usePersistentState } from "./utils/persistent-state";

const App = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
`;

function AppComponent() {
  const [subscriptions, setSubscriptions] = usePersistentState<string[]>(
    [],
    "subscriptions"
  );

  const routePathToPage: { [key: string]: JSX.Element } = {
    [ROUTES.Welcome]: <WelcomePage />,
    [ROUTES.Subscribe]: <SubscriptionPage />,
    [ROUTES.Default]: <Redirect to={ROUTES.Welcome} />,
  };

  return (
    <ThemeProvider theme={defaultTheme}>
      <CssBaseline />
      <App>
        <SubscriptionsContext.Provider
          value={{
            subscriptions,
            setSubscriptions,
          }}
        >
          <NavigationBarComponent />
          <PageTitleComponent />
          <Switch>
            {Object.keys(routePathToPage).map((routePath) => (
              <Route key={routePath} path={routePath}>
                {routePathToPage[routePath]}
              </Route>
            ))}
          </Switch>
        </SubscriptionsContext.Provider>
      </App>
    </ThemeProvider>
  );
}

export { AppComponent };
