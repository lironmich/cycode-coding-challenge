import { createMuiTheme } from "@material-ui/core";
import { Overrides as CoreOverrides } from "@material-ui/core/styles/overrides";
import { AutocompleteClassKey } from "@material-ui/lab";
import { CSSProperties } from "@material-ui/styles";

interface Overrides extends CoreOverrides {
  MuiAutocomplete?:
    | Partial<
        Record<AutocompleteClassKey, CSSProperties | (() => CSSProperties)>
      >
    | undefined;
}

const overrides: Overrides = {
  MuiAutocomplete: {
    inputRoot: {
      padding: "6px !important",
    },
  },
};

export const defaultTheme = createMuiTheme({ overrides });
