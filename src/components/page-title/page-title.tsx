import React from "react";
import { Typography } from "@material-ui/core";
import { useLocation } from "react-router-dom";
import styled from "styled-components";

const PageName = styled(Typography)`
  text-align: center;
  padding: 10px;
`;

function PageTitleComponent() {
  const location = useLocation();
  const pageName = location.pathname.substring(1);
  const uppercasePageName =
    pageName.charAt(0).toUpperCase() + pageName.slice(1);

  return <PageName variant="h6">{uppercasePageName}</PageName>;
}

export { PageTitleComponent };
