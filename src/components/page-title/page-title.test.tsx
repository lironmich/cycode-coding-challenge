import React from "react";
import { render, RenderResult } from "@testing-library/react";
import { PageTitleComponent } from "./page-title";

jest.mock("react-router-dom", () => ({
  useLocation: () => ({
    pathname: "/example",
  }),
}));

describe("page-title", () => {
  let renderResult: RenderResult;

  beforeEach(() => {
    renderResult = render(<PageTitleComponent />);
  });

  test("should contain upper-cased location path", async () => {
    const title = await renderResult.findByText("Example");

    expect(title).toBeTruthy();
  });
});
