import React, { useRef } from "react";
import { Chip, TextField } from "@material-ui/core";
import {
  Autocomplete,
  createFilterOptions,
  FilterOptionsState,
} from "@material-ui/lab";
import styled from "styled-components";

interface IAutocompleteProps {
  options: string[];
  values: string[];
  onChange: (values: string[]) => void;
  maxOptions?: number;
}

interface IAutocompleteOption {
  title: string;
  isCreated: boolean;
}

const TAGS_BUFFER = 60;
const TAG_WIDTH = 80;
const TAG_MARGIN = 3;

const Tag = styled(Chip)`
  width: ${TAG_WIDTH}px;
  margin: ${TAG_MARGIN}px;
`;

const filter = createFilterOptions<IAutocompleteOption>();

function AutocompleteComponent(props: IAutocompleteProps) {
  const ref = useRef<HTMLDivElement>();
  const selectedValues = props.values;

  const onChange = (event: object, values: IAutocompleteOption[]) => {
    const titles = values.map((_) => _.title);

    props.onChange(titles);
  };

  const options = props.options.map((option) => ({
    title: option,
    isCreated: false,
  }));

  const initialValues = props.values.map((value) => ({
    title: value,
    isCreated: false,
  }));

  const renderTags = (tags: IAutocompleteOption[]) => {
    const inputWidth = ref.current?.clientWidth;
    const tagsWidth = (TAG_WIDTH + 2 * TAG_MARGIN) * tags.length;

    if (inputWidth && tagsWidth + TAGS_BUFFER > inputWidth) {
      return <Tag label={`(${tags.length} more)`} />;
    }

    return (
      <div>
        {tags.map((tag) => (
          <React.Fragment key={tag.title}>
            <Tag label={tag.title} />
          </React.Fragment>
        ))}
      </div>
    );
  };

  const filterOptions = (
    options: IAutocompleteOption[],
    state: FilterOptionsState<IAutocompleteOption>
  ) => {
    const value = state.inputValue;
    const filtered = filter(options, state);

    const isValueAlreadySelected = selectedValues.some(
      (selectedValue) => selectedValue.toLowerCase() === value.toLowerCase()
    );

    const isValueAlreadyInFilter = filtered.some(
      (filteredValue) =>
        filteredValue.title.toLowerCase() === value.toLowerCase()
    );

    if (value && !isValueAlreadySelected && !isValueAlreadyInFilter) {
      filtered.unshift({
        title: value,
        isCreated: true,
      });
    }

    if (props.maxOptions) {
      filtered.splice(props.maxOptions);
    }

    return filtered;
  };

  return (
    <Autocomplete
      ref={ref}
      value={initialValues}
      multiple
      fullWidth
      filterSelectedOptions={true}
      autoHighlight={true}
      options={options}
      onChange={onChange}
      getOptionLabel={(option) => option.title}
      renderOption={(option) =>
        option.isCreated ? `Create '${option.title}'` : option.title
      }
      getOptionSelected={(option, value) => option.title === value.title}
      renderInput={(params) => (
        <TextField {...params} variant="outlined" label="subscriptions" />
      )}
      renderTags={renderTags}
      filterOptions={filterOptions}
      disablePortal={true}
    />
  );
}

export { AutocompleteComponent };
