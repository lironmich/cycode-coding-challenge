import React from "react";
import { render, RenderResult, fireEvent } from "@testing-library/react";
import { AutocompleteComponent } from "./autocomplete";

jest.mock("popper.js", () => {
  const PopperJS = jest.requireActual("popper.js");

  return class {
    static placements = PopperJS.placements;

    constructor() {
      return {
        destroy: () => {},
        scheduleUpdate: () => {},
      };
    }
  };
});

describe("autocomplete", () => {
  let renderResult: RenderResult;

  beforeEach(() => {
    renderResult = render(
      <AutocompleteComponent values={[]} options={[]} onChange={() => {}} />
    );
  });

  test("typing un-existing option should suggest creating a new one", async () => {
    const input = renderResult.container.querySelector(
      ".MuiInputBase-input"
    ) as HTMLElement;

    const notExistingValue = "not exist";

    input.focus();

    fireEvent.change(input, { target: { value: notExistingValue } });

    const creationOption = renderResult.findByText(
      `Create '${notExistingValue}'`
    );

    expect(creationOption).toBeTruthy();
  });
});
