import React, { ReactNode } from "react";
import styled from "styled-components";
import { FrameComponent } from "./frame";

interface IModalFrameProps {
  isOpen: boolean;
  children: ReactNode;
}

const ModalFrame = styled(FrameComponent)`
  position: fixed;
  top: 0;
  width: 100%;
  height: 100%;
`;

function ModalFrameComponent(props: IModalFrameProps) {
  if (!props.isOpen) {
    return null;
  }

  return <ModalFrame data-testid="modal-frame">{props.children}</ModalFrame>;
}

export { ModalFrameComponent };
