import React, { ReactNode } from "react";
import { create } from "jss";
import { jssPreset } from "@material-ui/core/styles";
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider, StylesProvider } from "@material-ui/core/styles";
import Frame, { FrameContextConsumer } from "react-frame-component";
import { StyleSheetManager } from "styled-components";

const theme = createMuiTheme({});

const CustomHead = () => {
  return (
    <>
      <meta charSet="utf-8" />
      <meta name="viewport" content="width=device-width,initial-scale=1" />
      <base target="_parent" />
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
      />
    </>
  );
};

const FrameComponent = (props: { children: ReactNode }) => {
  return (
    <Frame frameBorder={0} {...props} head={<CustomHead />}>
      <FrameContextConsumer>
        {({ document }) => {
          const jss = create({
            plugins: [...jssPreset().plugins],
            insertionPoint: document.head,
          });
          return (
            <StyleSheetManager target={document.head}>
              <StylesProvider jss={jss}>
                <ThemeProvider theme={theme}>{props.children}</ThemeProvider>
              </StylesProvider>
            </StyleSheetManager>
          );
        }}
      </FrameContextConsumer>
    </Frame>
  );
};

export { FrameComponent };
