import React from "react";
import { render, RenderResult } from "@testing-library/react";
import { NavigationBarComponent } from "./navigation-bar";
import { BrowserRouter } from "react-router-dom";

describe("navigation-bar", () => {
  let renderResult: RenderResult;

  beforeEach(() => {
    renderResult = render(
      <BrowserRouter>
        <NavigationBarComponent />
      </BrowserRouter>
    );
  });

  test("should contain project name", async () => {
    const projectName = await renderResult.findByText(
      "Cycode-coding-challenge"
    );

    expect(projectName).toBeTruthy();
  });

  test("should have welcome as a default selected tab", async () => {
    const welcomeText = await renderResult.findByText("welcome");
    const welcomeLink = welcomeText.closest("a") as HTMLAnchorElement;

    const href = welcomeLink.href;
    const selected = welcomeLink.getAttribute("aria-selected");

    expect(href).toContain("/welcome");
    expect(selected).toBe("true");
  });

  test("should have subscribe as a unselected tab", async () => {
    const subscriptionText = await renderResult.findByText("subscribe");
    const subscriptionLink = subscriptionText.closest("a") as HTMLAnchorElement;

    const href = subscriptionLink.href;
    const selected = subscriptionLink.getAttribute("aria-selected");

    expect(href).toContain("/subscribe");
    expect(selected).toBe("false");
  });

  test("clicking on subscribe should set is as a selected tab", async () => {
    const subscriptionText = await renderResult.findByText("subscribe");
    const subscriptionLink = subscriptionText.closest("a") as HTMLAnchorElement;

    subscriptionLink.click();

    const selected = subscriptionLink.getAttribute("aria-selected");

    expect(window.location.pathname).toBe("/subscribe");
    expect(selected).toBe("true");
  });
});
