import React from "react";
import {
  IconButton,
  AppBar,
  Toolbar,
  Typography,
  Tabs,
  Tab,
  withTheme,
} from "@material-ui/core";
import { HomeRounded } from "@material-ui/icons";
import { Route, Link } from "react-router-dom";
import styled from "styled-components";
import { ITheme } from "../../interfaces/ITheme";
import { ROUTES } from "../../consts/routes";

const CustomizedToolbar = styled(Toolbar)`
  justify-content: space-between;
`;

const CustomizedTypography = styled(Typography)`
  white-space: nowrap;
`;

const FlexContainer = styled.div`
  display: flex;
  align-items: center;
`;

const HomeIcon = styled(HomeRounded)`
  color: ${(props: { "icon-color": string }) => props["icon-color"]};
`;

const IconMargin = styled.span`
  flex: 0 0 20px;
`;

function NavigationBarComponent(props: ITheme) {
  const DEFAULT_TAB = ROUTES.Welcome;

  const getTabsValue = (path: string) => {
    return (
      [ROUTES.Welcome, ROUTES.Subscribe].find((_) => _ === path) || DEFAULT_TAB
    );
  };

  return (
    <Route
      path="/"
      render={({ location }) => (
        <AppBar position="static">
          <CustomizedToolbar>
            <FlexContainer>
              <IconButton component={Link} to={ROUTES.Welcome}>
                <HomeIcon icon-color={props.theme.palette.common.white} />
              </IconButton>
              <IconMargin />
              <CustomizedTypography variant="h6">
                Cycode-coding-challenge
              </CustomizedTypography>
            </FlexContainer>

            <Tabs value={getTabsValue(location.pathname)}>
              <Tab
                label="welcome"
                value={ROUTES.Welcome}
                component={Link}
                to={ROUTES.Welcome}
              />
              <Tab
                value={ROUTES.Subscribe}
                label="subscribe"
                component={Link}
                to={ROUTES.Subscribe}
              />
            </Tabs>
          </CustomizedToolbar>
        </AppBar>
      )}
    />
  );
}

const _NavigationBarComponent = withTheme(NavigationBarComponent);

export { _NavigationBarComponent as NavigationBarComponent };
